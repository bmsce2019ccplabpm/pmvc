#include<stdio.h>
int main()
{
 int a,b;
 printf("Enter a and b\n");
 scanf("%d%d",&a,&b);
 printf("Before Swap a=%d b=%d\n",a,b);
 swap(&a,&b);
 printf("After Swap a=%d b=%d\n",a,b);
 return 0;
}
void swap(int *a,int *b)
{
 int t;
 t=*a;
 *a=*b;
 *b=t;
}